package com.mycompany.imagej;

import java.util.ArrayList;

public class PixGroup implements Comparable<PixGroup>{
    private ArrayList<Pixel> pixels = new ArrayList<Pixel>();
    public PixGroup(){
         
    }
    public void addPixel(Pixel pixel){
        pixels.add(pixel);
    }

    public int getNumberofPixels(){
        return pixels.size();
    }

    // overriden compareTo to allow sorting of groups by 
    // number of pixels contained 
    public int compareTo(PixGroup compareGroup){
        return this.getNumberofPixels() - compareGroup.getNumberofPixels();
    }

    public Pixel getCentrePixel(){
        double start_x, start_y;
        double end_x, end_y;
        end_x = start_x = pixels.get(0).getxLoc();
        end_y = start_y = pixels.get(0).getyLoc();
        for (int i=1;i<pixels.size();i++){
            int _xloc = pixels.get(i).getxLoc();
            int _yloc = pixels.get(i).getyLoc();
            if (_xloc>end_x){
                end_x = _xloc;
            } else if (_xloc<start_x){
                start_x = _xloc;
            }
            if (_yloc >end_y){
                end_y = _yloc;
            } else if (_yloc<start_y){
                start_y = _yloc;
            }
        }
        double xdiff, ydiff, centre_x, centre_y;
        xdiff = end_x-start_x;
        ydiff = end_y-start_y;
        centre_x = start_x+(xdiff/2);
        centre_y = start_y+(ydiff/2);
        PixelMap pixelMap = PixelMap.getInstance();
        Pixel returnPixel =  pixelMap.getPixel((int)centre_x,(int)centre_y);
        return returnPixel;
    }
}