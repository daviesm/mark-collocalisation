package com.mycompany.imagej;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import ij.ImagePlus;
import ij.WindowManager;
import ij.plugin.ChannelSplitter;
import ij.plugin.PlugIn;
import ij.process.ImageProcessor;

/** Builds a String containg all of the values in each channel of an image
 *  seperated by newline characters in csv format
 */

public class Pixel_Grabber implements PlugIn{

    public void run(String str){
        File fileToSave = null;
        ImagePlus imp = WindowManager.getCurrentImage();
        ImagePlus[] channels = ChannelSplitter.split(imp);
        StringBuilder stringBuilder = new StringBuilder(); 
        for (int i=0;i<channels.length;i++){
            for (float value:grabPixels(channels[i].getProcessor())){
                stringBuilder.append(value);
                stringBuilder.append(", ");
            }
            //remove last comma and space and add newline character 
            stringBuilder.setLength(stringBuilder.length()-2);
            stringBuilder.append("\n");
        }
        String data = stringBuilder.toString();

        // Get file path for save with file chooser 
        JFrame parentFrame = new JFrame();
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Save Pixel Data");
        
        int userSelection = fileChooser.showSaveDialog(parentFrame);

        if (userSelection == JFileChooser.APPROVE_OPTION){
            fileToSave = fileChooser.getSelectedFile();
        }

        // Set up writer and write to file 
        BufferedWriter bWriter = null; 
        FileWriter fileWriter = null; 
        try { 

                fileWriter = new FileWriter(fileToSave); 
                bWriter = new BufferedWriter(fileWriter); 
                bWriter.write(data); 
        } catch (IOException ex) { 
                ex.printStackTrace(); 
        } finally { 
                try { 
                        if(bWriter!=null) 
                                bWriter.close(); 
                }catch(Exception ex){ 
                        System.out.println("Error in closing the BufferedWriter"+ex); 
                } 
                System.out.println("Data written to file"); 
        }


        return;
    }

    // takes imageprocessor and extracts all pixel values into a float[] (height then width)
    public float[] grabPixels(ImageProcessor ip){
        float[] pixelData = new float[ip.getWidth()*ip.getHeight()];
        for (int i=0;i<ip.getHeight();i++){
            for (int j=0;j<ip.getWidth();j++){
                pixelData[(i*ip.getWidth())+j] = ip.getf(i,j);
            }
        }
        return pixelData;
    }
}