package com.mycompany.imagej;

import java.awt.Color;
import java.awt.image.IndexColorModel;
import java.util.LinkedHashMap;

import ij.CompositeImage;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import ij.process.ImageProcessor;
import ij.process.LUT;
/** Sets the colors of an image to those requested by a generic dialog screen. 
 * Colored image is saved as an attribute of the object that can be accessed by getImage()
 * allowing calling processes to check whether process was successful before accepting Image.
 */

public class Set_Channel_Colors implements PlugIn{

    private ImagePlus generatedImp;
    private boolean dialogWasCancelled = false;
    
    public Set_Channel_Colors(){
        generatedImp = null;
    }
    public void run(String str){
        ImagePlus imp = WindowManager.getCurrentImage();
        if(imp.getBitDepth()==24){
            IJ.error("Set Channel Colors", "Source image cannot be RGB");
        }
        this.setChannelColors(imp);
        if(!dialogWasCancelled){
            generatedImp.show();
        }
    }

    /** Call after creating an instance to create an correctly colored composite image 
     * accessable by getImage() and using a generic dialog. If the dialog was cancelled dialogCancelled()
     * will return true.
     */
    public void setChannelColors (ImagePlus imp){
        imp = imp.duplicate();
        GenericDialog gd = new GenericDialog("Set Channel Colors");
        LinkedHashMap<String,Color> colorMap = new LinkedHashMap<String,Color>();
        {
            colorMap.put("Red",Color.red);
            colorMap.put("Green",Color.green);
            colorMap.put("Blue", Color.blue);
            colorMap.put("Grey",Color.white);
            colorMap.put("Cyan",Color.cyan);
            colorMap.put("Magenta",Color.magenta);
            colorMap.put("Yellow",Color.yellow);
        }
        String[] colorMapKeyArray = colorMap.keySet().toArray(new String[colorMap.keySet().size()]);
        for (int c=0; c<imp.getNChannels();c++){
            gd.addChoice("Channel" + Integer.toString(c+1), colorMapKeyArray, colorMapKeyArray[0]);
        }
        gd.showDialog();

        dialogWasCancelled = false;
        if(gd.wasCanceled()){
            dialogWasCancelled = true; 
            return;
        }

        Color[] colors = new Color[imp.getNChannels()];
        for (int i=0;i<colors.length;i++){
            String choice = gd.getNextChoice();
            colors[i]=colorMap.get(choice);
        }
        ImagePlus newimp = createCompositeImage(imp, colors);
        generatedImp = newimp;
    }

    public ImagePlus getImage(){
        return generatedImp;
    }

    public boolean dialogCancelled(){
        return dialogWasCancelled;
    }
    /** Returns an CompositeImage with channels set to the colors given by the Color[]
     * in the given order.
     * If an entry in the Color[] is null the original lut of that channel is used 
     * Method can be called without instantiation */
    public static ImagePlus createCompositeImage(ImagePlus imp, Color[] colors){
        int slices,channels,frames;

        if(imp.getBitDepth()==24){
            throw new IllegalArgumentException("Parsed image cannot be RGB");
        }

        slices = imp.getNSlices();
        frames = imp.getNFrames();
        channels = imp.getNChannels();
        
        ImageStack stack = imp.getStack();
        ImagePlus newImp = new ImagePlus(imp.getTitle(),stack);
        newImp.setDimensions(channels, slices, frames);
        newImp.setCalibration(imp.getCalibration());
        newImp = new CompositeImage(newImp,IJ.COMPOSITE);

        for (int c=0; c<channels; c++){
            imp.setC(c+1);
            ImageProcessor ip = imp.getProcessor();
            LUT lut = null;
            if (c<colors.length && colors[c]!=null){
                lut = LUT.createLutFromColor(colors[c]);
                lut.max = ip.getMax();
                lut.min = ip.getMin();
            } else {
                lut = new LUT((IndexColorModel)ip.getColorModel(),ip.getMin(),ip.getMax());
            }
            ((CompositeImage)newImp).setChannelLut(lut,c+1);

        }
        newImp.setOpenAsHyperStack(true);

        return newImp;
   }


}