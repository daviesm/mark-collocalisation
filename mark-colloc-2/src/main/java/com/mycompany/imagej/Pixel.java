package com.mycompany.imagej;

public class Pixel {
    private PixGroup thisGroup;
    private int xloc;
    private int yloc;
    private boolean aboveThreshold;
    private PixelMap pixelMap;
    private boolean inspected;

    public Pixel () {
        pixelMap = PixelMap.getInstance();
        inspected = false;
    }
    public Pixel (int x, int y){
        xloc = x;
        yloc = y;
        pixelMap = PixelMap.getInstance();
        inspected = false;
    }
    private void addToGroup (){
        GroupController groupController = GroupController.getInstance();
        groupController.addToGroup(this);
    }
    // check if this pixel is high if it is add to group
    // then recursively inspect all adjacent pixels 
    public boolean inspect(int count){
        boolean _inspected = inspected;
        inspected = true;
        if(aboveThreshold && !_inspected){
            addToGroup();
            for(int i =0;i<3;i++){
                for(int j=0; j<3; j++){
                    Pixel checkPix = pixelMap.getPixel(xloc+(i-1), yloc+j-1);
                    if (checkPix != null){
                        try{
                            checkPix.inspect(++count);
                        }catch (StackOverflowError ex){
                            System.out.println(count);
                            System.exit(0);
                        }
                    
                    }
                }

            }
        }
        return aboveThreshold;
    }

    // getters and setters
    public PixGroup getGroup (){
        return thisGroup;
    }

    public void setGroup(PixGroup group){
        thisGroup = group;
    }

    public int getxLoc(){
        return xloc;
    }

    public int getyLoc(){
        return yloc;
    }

    public void setValue(boolean bool){
        aboveThreshold = bool;
    }

    public boolean getValue(){
        return aboveThreshold;
    }

    public boolean isInspected(){return inspected;}
}