package com.mycompany.imagej;

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import ij.*;
import ij.process.*;
import ij.gui.*;
import java.awt.*;
import java.awt.geom.Point2D;

import java.util.ArrayList;
import java.util.Collections;

import fiji.util.ArrowShape;
import fiji.util.ArrowShape.ArrowStyle;

/** This plugin converts confocal images into a montage
 * utilising the montagemaker plugin
 */
public class Coloc_Marker implements PlugIn {

	private ImagePlus imp;
	private int imageWidth,imageHeight;
	private Overlay overlay;
	ImageProcessor[] colocIPs;
	int colocChannel1, colocChannel2, nGroupsToLabel;
	int[] thresholds, colocChannels;

    public void run(String arg){
	   imp = WindowManager.getCurrentImage();
	   imp = imp.duplicate();

	   Set_Channel_Colors colorSetter = new Set_Channel_Colors();
	   colorSetter.setChannelColors(imp);
	   imp = colorSetter.getImage();
	   
	   ArrayList<String> channelNames = new ArrayList<String>();
	   for(int i=0;i<imp.getNChannels();i++){
		   channelNames.add("Channel "+ Integer.toString(1+i));
	   }
	   GenericDialog gd = new GenericDialog("Mark colocalisation");
	   gd.addChoice("Coloc Channel 1", channelNames.toArray(new String[0]), channelNames.get(0));
	   gd.addChoice("Coloc Channel 2", channelNames.toArray(new String[0]), channelNames.get(1));
	   gd.addStringField("Channel 1 Threshold", "50");
	   gd.addStringField("Channel 2 Threshold", "50");
	   gd.addStringField("No. Groups to Label", "10");
	   gd.showDialog();

	   if (gd.wasCanceled()){
		   return;
	   }

	   thresholds = new int[2];
	   for (int i=0;i<2;i++){
		   String thresholdString = gd.getNextString();
		   if(!isInterger(thresholdString)){
			   IJ.error("Threshold must be integer value");
			   return;
		   }
		   thresholds[i]=Integer.parseInt(thresholdString);
	   }

	   String sGroupsToLabel = gd.getNextString();
	   if(!isInterger(sGroupsToLabel)){
		   IJ.error("Number of groups to label must be > 0 and a valid integer");
		   return;
	   }
	   nGroupsToLabel = Integer.parseInt(sGroupsToLabel);

	   colocChannels = new int[2];
	   colocChannels[0] = gd.getNextChoiceIndex();
	   colocChannels[1] = gd.getNextChoiceIndex();
	   colocChannel1 = colocChannels[0];
	   colocChannel2 = colocChannels[1];
	   getPixelData();
//	   if (imp.getProcessor() instanceof ColorProcessor){
//		   
//	   }
	   ImageStack newStack = new ImageStack(imp.getWidth(),imp.getHeight());
	   for (int i=0; i<imp.getNChannels(); i++){
			imp.setC(1+i);
			ImageProcessor ip = imp.getChannelProcessor().convertToRGB();
			boolean colocChannel = false;  
			for(int n=0; n<colocChannels.length;n++){
				if(i == colocChannels[n]) colocChannel = true;
			}
			if(colocChannel && overlay!=null){
				ip.setColor(Color.white);
				for(int nRoi=0; nRoi<overlay.size(); nRoi++){
					ip.resetRoi();
					ip.setRoi(overlay.get(nRoi));
					ip.draw(overlay.get(nRoi));
					ip.resetRoi();
				}
			}
			newStack.addSlice(ip);
	   }
	   if(overlay==null) IJ.error("Colocalisation Finder", "No areas of colocalisation found");

	   ImagePlus newImp = new ImagePlus("drawn", newStack);
	   newImp.setDimensions(imp.getNChannels(), imp.getNSlices(), imp.getNFrames());
	   newImp.setCalibration(imp.getCalibration());
	   imp = newImp;
	   _Montage montage = new _Montage(imp);
	   boolean montageMade = montage.makeMontage();
	   if(!montageMade){
		   imp.show();
	   }
	}

	public void getPixelData(){
		
        if (imp == null){
            error("No images are open, open an image to run this plugin");
            return;
        }
		
		if (imp.getNChannels()<2){
			error("Image has only one channel");
			return;
		}
		// split the image and get the titles xof the channels 
		ImageProcessor ips[] = new ImageProcessor[imp.getStackSize()];
		for (int i=0;i<ips.length;i++){
			ips[i] = imp.getStack().getProcessor(i+1);
		}
		
		// get the two channels to check for colocalisation 
		colocIPs = new ImageProcessor[] {ips[colocChannel1],ips[colocChannel2]};
		
		//check selected images are the same height and width 
		if(colocIPs[0].getHeight() != colocIPs[1].getHeight()||
			colocIPs[0].getWidth() != colocIPs[1].getWidth()){
				System.out.println("Image Processors are not the same height/width");
		}

		// set height and width 
		imageHeight = colocIPs[0].getHeight();
		imageWidth = colocIPs[0].getWidth();

		// get pixel map instance and setup with width and 
		// height of image
		PixelMap pixelMap = PixelMap.getInstance();
		pixelMap.setup(colocIPs[0].getHeight(),colocIPs[0].getWidth());

		float threshold1 = 10;
		float threshold2 = 10;

		for (int i=0;i<colocIPs[0].getHeight();i++){
			for (int j=0;j<colocIPs[0].getWidth();j++){
				float val1 = colocIPs[0].getf(i,j);
				float val2 = colocIPs[1].getf(i,j);
				boolean aboveThreshold = (val1>threshold1 && val2>threshold2);
				pixelMap.getPixel(i, j).setValue(aboveThreshold);
				
			}
		}

		// start group controller 
		GroupController groupController = GroupController.getInstance();
		for (int i=0;i<pixelMap.getHeight();i++){
			for (int j=0; j<pixelMap.getWidth();j++){
				boolean pixelWasHigh = pixelMap.getPixel(i, j).inspect(0);
				if(pixelWasHigh){
					groupController.resetCurrentGroup();
				}
			}
		}
 
		// get the group array from the group controller 
		// then sort the groups in the list by the number of
		// pixels each contains 
		ArrayList<PixGroup> grouplList = groupController.getGroupList();
		Collections.sort(grouplList, Collections.reverseOrder());
		for (int i=0;i<(nGroupsToLabel>grouplList.size()?grouplList.size():nGroupsToLabel);i++){
			int xloc = grouplList.get(i).getCentrePixel().getxLoc();
			int yloc = grouplList.get(i).getCentrePixel().getyLoc();
			placeArrow(xloc, yloc);
		}
		return;
	}

	// draw arrows onto the overlay with the tip at the specified x and y locations
	// the tail and head of the arrow are relative to the size of the image 
	private void placeArrow(int xloc,int yloc){
		if(overlay==null){
			overlay= new Overlay();
			imp.setOverlay(overlay);
		}
		// find the length of the arrow based on % of 
		// image width 
		double arrowLength = (double)imageWidth/10;
		ArrowShape arrow = new ArrowShape(ArrowStyle.DELTA,imageWidth/30);
		arrow.setStartPoint(new Point2D.Double(xloc-arrowLength,yloc));
		arrow.setEndPoint(new Point2D.Double(xloc,yloc));	
		ShapeRoi arrow_roi = new ShapeRoi(arrow);

		overlay.add(arrow_roi);
		

		return;

//		String[] titles = new String[channels.length+1];
//		for (int i=0; i<channels.length; i++){
//			ImagePlus c = channels[i];
//			titles[i] = c.getTitle();
//		}
//		//Fill last slot in array with 'none' string
//		titles[titles.length-1]= none;
//
////		ContrastAdjuster cAdjuster = new ContrastAdjuster();
////		Thread thread = new Thread(cAdjuster,"contrastAdjuster");
////		cAdjuster.run("");
////		thread.start();
////		try{
////			thread.join();
////		} catch (InterruptedException e){
////			//do nothing
////		}
//
//		//for (ImagePlus channel : channels) {
//		//	channel.show();
//		//}
//
//
//		GenericDialog gd = new GenericDialog("Confocal Montage");
//		String[] colors = {"Red","Green","Blue","Grey","Cyan","Magenta","Yellow"};
//		for(int i=0;i<colors.length;i++){
//			gd.addChoice(colors[i], titles, i<titles.length?titles[i]:none);
//			gd.addCheckbox("Ingore?", false);
//		}
//		gd.showDialog();
        
    }

    void error(String msg){
        IJ.error("Confocal Montage", msg);
	}
	
	boolean isInterger(String s){
		if(s.length()<1) return false;

		for(int i=0;i<s.length();i++){
			if(!Character.isDigit(s.charAt(i)) || s.charAt(i)=='.'){
				return false;
			}
		}

		return true;
	}
}



