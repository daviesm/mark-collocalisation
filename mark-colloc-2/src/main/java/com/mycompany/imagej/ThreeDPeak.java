package com.mycompany.imagej;

import java.util.List;

import ij.process.ImageProcessor;

import java.util.Arrays;
import java.util.ArrayList;

/**;
 * Holds a list of location coordinates that
 * represent colocalising pixels. Created by getColocPeaks
 * Can be used to:
 *      get center point of peak 
 *      get size of peak 
 * is created with the ImageProcessor that it maps to 
 */
public class ThreeDPeak extends Peak{

        private List<Coloc_Marker2.TwoDPeak> twoDPeakList;
        private ImageProcessor linkedIp;


        protected ThreeDPeak(ImageProcessor linkedImgProcessor){
            this.twoDPeakList = new ArrayList<Coloc_Marker2.TwoDPeak>();
            this.locationList = new ArrayList<int[]>(); 
            this.linkedIp = linkedImgProcessor;
        }

        /**
         * Checks if the location sent already exists in this peak
         * if it does not it adds it to the locationList
         * @param location
         */
        protected void addLocation(int[] location){
            boolean locationExists = false;
            for (int[] localLocation : locationList){
                if(Arrays.equals(localLocation, location)){
                    locationExists = true;
                }
            }
            if(!locationExists){
                this.locationList.add(location);
            }
        }

        protected void add2DPeak(Coloc_Marker2.TwoDPeak twoDPeak){
            twoDPeakList.add(twoDPeak);
        }

        protected int[] getPeakMax(){
            return getPeakMax(this.linkedIp);
        }

        /**
         * Returns the location of the pixel at the 'top' of the peak.
         * <p> 
         * It will first look for the highest value pixel, if there are 
         * multiple pixels with this value it will sum the neighboring 
         * pixels and return the location of the pixel with the highest 
         * value neighbors. If there are still multiple pixels it will
         * return the pixel closest to the geometric center of the peak
         * @param ipCombinedChannels the ImageProcessor from which to get peak values
         * @return an int[] containing the location of the pixel at the 'top' of the peak
         */
        protected int[] getPeakMax (ImageProcessor ipCombinedChannels){
            int[] maxLocation = new int[2];
            int maxValue = 0;
            List<int[]> sameIntensityPixels = new ArrayList<int[]>();
            // for each in the 3d peak get the value of the pixel at that location
            // return the location with the highest value 
            for (int i =0;i<this.locationList.size();i++){
                int pixelVal = ipCombinedChannels.getPixel(locationList.get(i)[1], locationList.get(i)[0]);
                if (pixelVal>maxValue){
                    maxValue = pixelVal;
                    maxLocation = locationList.get(i);
                    sameIntensityPixels.clear();sameIntensityPixels.add(maxLocation);
                } else if (pixelVal == maxValue){
                    sameIntensityPixels.add(locationList.get(i));
                }
            }

            //if there are more than one location with the highest values then
            // the center is the pixel where the sum of the surrounding pixels is higest 
            // if this is still the same return the one closest to the 'center' of the peak

            if(sameIntensityPixels.size()>1){ //ie more than 1 location with maxValue
                maxValue = 0;
                List<int[]> stillSameIntensityPixels = new ArrayList<int[]>();
                //for each location in sameIntensity Pixels
                for (int[] location : sameIntensityPixels){
                    int sumOfNeighbors = 0;
                    //sum the surrounding pixels 3x3 block
                    for (int i=0; i<3;i++){
                        for (int u=0; u<3; u++){
                            sumOfNeighbors += ipCombinedChannels.getPixel((location[1]+i-1), (location[0]+u-1));
                        }
                    }

                    // check if the highest 
                    if (sumOfNeighbors > maxValue){
                        maxValue = sumOfNeighbors;
                        maxLocation = location;
                        stillSameIntensityPixels.clear();stillSameIntensityPixels.add(location);
                    } else if (sumOfNeighbors == maxValue){
                        stillSameIntensityPixels.add(location);
                    }
                }

                // if there are still competeing pixels return pixel closest to the center
                // of location list
                if (stillSameIntensityPixels.size()>1){
                    int[] center = this.getCenter(ipCombinedChannels);
                    double score = -1;
                    int[] closestToCenter = new int[2]; 
                    for (int[] location : stillSameIntensityPixels){
                        double dCenterX = Math.pow((center[1] - location[1]),2);
                        double dCenterY = Math.pow((center[0] - location[0]),2);
                        double tempScore = dCenterX+dCenterY;
                        if (score == -1){
                            score = tempScore;
                            closestToCenter = location;
                        }else if (tempScore<score){
                            score = tempScore;
                            closestToCenter = location;
                        }
                        if(score==0){
                            return closestToCenter;
                        }

                    }                  
                    
                    return closestToCenter;
                } else {
                    return maxLocation;
                }
            } else {
                return maxLocation;
            }
        }

        protected int[] getCenter(ImageProcessor ipCombinedChannels){

            int maxX = 0,maxY = 0, minX=(int)ipCombinedChannels.getWidth(),minY=(int)ipCombinedChannels.getHeight();

            for(int[] location : this.locationList){
                if(location[1]< minX){
                    minX = location[1];
                } else if( location [1]>maxX){
                    maxX = location[1];
                }

                if(location[0] <minY){
                    minY = location[0];
                } else if (location[0]>maxY){
                    maxY = location[0];                   
                }
            }

            int locationX = (maxX + minX)/2;
            int locationY = (maxY+minY)/2;

            return new int[] {locationY,locationX};
        }
    }

