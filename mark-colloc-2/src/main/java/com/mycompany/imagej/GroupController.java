package com.mycompany.imagej;

import java.util.ArrayList;

public class GroupController {
    private static GroupController instance = null;
    private PixGroup currentGroup = null;
    private ArrayList<PixGroup> pixGroups = new ArrayList<PixGroup>();
    private GroupController () {
        // private to render this class a singleton
        // only accessable via getInstance
        currentGroup = new PixGroup();
    }

    public static GroupController getInstance(){
        if(instance == null){
            instance = new GroupController();
        }
        return instance;
    }

    public void addToGroup (Pixel pixel){
        currentGroup.addPixel(pixel);
        pixel.setGroup(currentGroup);
    }

    /** If the current group is greater than the minimum pixels
     * the group will be stored and a new current group created, otherwise
     * the current group is reset
     */
    public void resetCurrentGroup(){
        if (currentGroup.getNumberofPixels() > 5){
            pixGroups.add(currentGroup);
        }
        currentGroup = new PixGroup();
    }

    public int getNumberofGroups(){
        return pixGroups.size();
    }

    public ArrayList<PixGroup> getGroupList(){
        return pixGroups;
    }
}