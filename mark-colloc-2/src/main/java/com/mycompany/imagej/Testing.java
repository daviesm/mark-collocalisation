package com.mycompany.imagej;

import java.io.IOException;

import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import loci.formats.FormatException;
import loci.plugins.BF;

public class Testing{
        /**
	 * Main method for debugging.
	 *
	 * For debugging, it is convenient to have a method that starts ImageJ, loads
	 * an image and calls the plugin, e.g. after setting breakpoints.
	 *
	 * @param args unused
	 */
    public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = Coloc_Marker2.class;
		String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring("file:".length(), url.length() - clazz.getName().length() - ".class".length());
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();
//		String msg = "hello";
//		OpenDialog od = new OpenDialog("Open Image");
//		String dir = od.getDirectory();
//		String name = od.getFileName();
//		String id = dir + name;
//		String testFilePath = Confocal_Montage.class.getClassLoader().getResource("test-file.").toString();
		String path = Coloc_Marker.class.getClassLoader().getResource("test3.tif").getFile();
		System.out.println(path);
//		try {
//			BufferedReader br = new BufferedReader(new FileReader(file));
//			String str;
//			while ((str = br.readLine()) != null){
//				System.out.println(str);
//			}
//			br.close();
//		}
//		catch (FileNotFoundException ex){
//			System.out.println("Unable to open the file");
//		}
//		catch (IOException ex){
//			System.out.println("error reading file");
//		}
		// open the sample czi file 
		try {
			ImagePlus[] imps = BF.openImagePlus(path);
			for (ImagePlus imp :imps) imp.show();
		}
		catch(IOException exc){
			IJ.error("sorry an IO error occurred: " + exc.getMessage());
		}
		catch(FormatException exc){
			IJ.error("Sorry an error occured: " + exc.getMessage());
		}
		catch(NumberFormatException exc){
			exc.printStackTrace(System.out);
		}
		//ImagePlus image = IJ.openImage(System.getProperty("user.dir")+"\\0min_sonicate1.czi");
		//image.show();

		// run the plugin
		IJ.runPlugIn(clazz.getName(), "");
	}
}