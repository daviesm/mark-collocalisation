package com.mycompany.imagej;

import java.awt.Color;
import java.util.LinkedHashMap;
import java.util.Vector;

import ij.CompositeImage;
import ij.ImagePlus;
import ij.ImageStack;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.measure.Calibration;
import ij.plugin.MontageMaker;
import ij.plugin.PlugIn;
import ij.process.Blitter;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;

public class _Montage implements PlugIn{
    ImagePlus imp; 
    int barWidth = 1;
    public _Montage(){
        imp = null;
    }
    public _Montage(ImagePlus imp){
        this.imp = imp;
    } 
    public void run(String str){
        imp = WindowManager.getCurrentImage();
        makeMontage();
    }
    public boolean makeMontage(){

        if(imp==null)return false;

        int channels;
        Vector<Boolean> includeInMontageResults;
        int scaleBarResult;
        boolean generateMergedResult;
        boolean setChannelColors = false;
        
        GenericDialog gd = new GenericDialog("Make Montage");

        channels = imp.getNChannels();
        for(int c=0;c<channels;c++){
            gd.addCheckbox("Include Channel"+Integer.toString(c+1), true);
        }
        gd.addMessage("Scale Bar");
        LinkedHashMap<String,Integer> scaleBarOptions = new LinkedHashMap<String,Integer>();
        {
            scaleBarOptions.put("None",0);
            scaleBarOptions.put("First",1);
            scaleBarOptions.put("Last", 2);
            scaleBarOptions.put("All",3);
        }
        gd.addChoice("Label channels: ", scaleBarOptions.keySet().toArray(new String[scaleBarOptions.size()]), scaleBarOptions.keySet().toArray(new String[scaleBarOptions.size()])[0]);
        gd.addMessage("Merge");
        gd.addCheckbox("Generate merged channel", true);
        if (imp.getType()!=ImagePlus.COLOR_RGB){
            gd.addMessage("Channel Colors");
            gd.addCheckbox("Set Colors", true);
        }
        gd.showDialog();

        if(gd.wasCanceled())return false;


        // List of channels to include in montage.
        includeInMontageResults = new Vector<Boolean>();
        for (int i=0; i<channels;i++){
            includeInMontageResults.add(gd.getNextBoolean());
        }
        
        scaleBarResult = scaleBarOptions.get(gd.getNextChoice());
        generateMergedResult = gd.getNextBoolean();
        if(gd.getCheckboxes().size() > channels+1){
            setChannelColors = gd.getNextBoolean();
        }

        if (setChannelColors && imp.getType()!=ImagePlus.COLOR_RGB){
            Set_Channel_Colors channelSetter = new Set_Channel_Colors();
            channelSetter.setChannelColors(imp);
            if(!channelSetter.dialogCancelled()&&channelSetter.getImage()!=null){
                imp = channelSetter.getImage();
            }
        }

        ImagePlus imp2 = new ImagePlus();
        if (generateMergedResult){
            imp2 = addMerged(imp);
        } else { //generate RGB stack of imp 
            ImageStack stack = new ImageStack(imp.getWidth(), imp.getHeight());
            ImageStack oldStack = imp.getStack();
            for (int c=0; c<imp.getStackSize();c++){
                stack.addSlice(oldStack.getProcessor(c+1).convertToRGB());
            }
            imp2.setStack(stack, imp.getNChannels(), imp.getNSlices(), imp.getNFrames());
            imp2.setCalibration(imp.getCalibration());
        }

        ImageStack stackWithUnwantedRemoved = new ImageStack(imp2.getWidth(),imp2.getHeight());
        imp2.setPosition(1);
        for (int i=0;i<imp2.getNChannels(); i++){
            if (i<includeInMontageResults.size()){
                if(includeInMontageResults.get(i)){
                    stackWithUnwantedRemoved.addSlice(imp2.getStack().getProcessor(i+1));
                }
            } else {
                stackWithUnwantedRemoved.addSlice(imp2.getStack().getProcessor(i+1));
            }

        }
        imp2.setStack(stackWithUnwantedRemoved);

        switch (scaleBarResult){
            case 0:
                break;
            case 1:
                imp2.setPosition(1,0,0);
                drawBar(imp2.getProcessor(), imp2.getCalibration());
                break;
            case 2:
                imp2.setPosition(imp2.getNChannels(),0,0);
                drawBar(imp2.getProcessor(), imp2.getCalibration());
                break;
            case 3:
                for(int i=0; i<imp2.getNChannels();i++){
                    imp2.setPosition(i+1, 0, 0);
                    drawBar(imp2.getProcessor(), imp2.getCalibration());
                }
                break;
        }
        imp2.setPosition(1);

        int spacer  = (int)(imp2.getWidth()/10);
        MontageMaker montageMaker = new MontageMaker();
        ImagePlus montage = montageMaker.makeMontage2(imp2, imp2.getStackSize(), 1, 1, 1, imp2.getStackSize(), 1, spacer, false);
        montage.show();
        return true;
    }


    private void drawBar(ImageProcessor ip, Calibration cal){
        int barWidthInPixels = (int)((double)barWidth/cal.pixelWidth);
        // +1 ensures the height is never zero, mainly important for testing 
        int barHeightInPixels = (int)((double)ip.getHeight()*0.001)+1;
        int height = imp.getHeight();
        int width = imp.getWidth();
        int fraction = 20;
        int x = (width - (width/fraction))-barWidthInPixels;
        int y = (height - (height/fraction))-barHeightInPixels;

        ip.resetRoi();
        ip.setColor(Color.white);
        ip.setRoi(x, y, barWidthInPixels, barHeightInPixels);
        ip.fill();
        ip.resetRoi();
    }

    public ImagePlus addMerged (ImagePlus parsedImp){
        imp = parsedImp.duplicate();
        if (imp.getType() == ImagePlus.COLOR_RGB){
            int width = imp.getWidth();
            int height = imp.getHeight();

            ImageProcessor mergedIP = new ColorProcessor(width, height);
            for(int i=0; i<imp.getStackSize();i++){
                mergedIP.copyBits(imp.getStack().getProcessor(i+1), 0, 0, Blitter.ADD);
            }
            imp.getStack().addSlice(mergedIP);
            return imp;            
       }
        if (!(imp instanceof CompositeImage)){
            imp = new CompositeImage(imp, 1);
        }
        imp.setDisplayMode(1);
        ImagePlus newImp = new ImagePlus(imp.getTitle());
        ImageStack newStack = new ImageStack(imp.getWidth(), imp.getHeight());
        imp.setC(1);
        for (int i=0; i<imp.getNChannels();i++){
            newStack.addSlice(imp.getChannelProcessor().convertToRGB());
            imp.setC(imp.getC()+1);
        }
        newStack.addSlice(imp.flatten().getProcessor());
        newImp.setStack(newStack, newStack.getSize(), 1, 1);
        newImp.setCalibration(imp.getCalibration());
        return newImp;


    }
    
} 