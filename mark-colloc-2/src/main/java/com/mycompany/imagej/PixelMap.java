package com.mycompany.imagej;

public class PixelMap{ 

    // singleton constructor
    private static PixelMap instance = null;
    private PixelMap(){
        // empty
    }
    public static PixelMap getInstance(){
        if(instance == null){
            instance = new PixelMap();
        }
        return instance;
    }

    private Pixel[][] map = null;

    public void setup(int height, int width){
        map = new Pixel[height][width];
        for(int i=0; i<height;i++){
            for(int j=0;j<width;j++){
                map[i][j] = new Pixel(i,j);
            }
        }
    }

    public Pixel getPixel(int yloc, int xloc){
        try {
            return map[yloc][xloc];
        } catch (IndexOutOfBoundsException exception){
            return null;
        }
    }

    public int getHeight(){
        return map.length;
    }

    public int getWidth(){
        return map[0].length;
    }
}