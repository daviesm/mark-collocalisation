package com.mycompany.imagej;

import java.util.List;

/**
 * Abstract superclass for peak objects providing access
 * to a List<int[]> location List and a getLocationList 
 * method
 */

abstract class Peak{

    protected List<int[]> locationList;

    // getter
    /**
     * Gets the list containing the locations of this peak
     * 
     * @return this peak-list
     */
    public List<int[]> getLocationList(){
        return locationList;
    }

}