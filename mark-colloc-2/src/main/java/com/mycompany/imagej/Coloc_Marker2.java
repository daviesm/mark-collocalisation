package com.mycompany.imagej;

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import ij.*;
import ij.process.*;
import ij.gui.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.nio.channels.Channel;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileSystemView;

import ij.plugin.ChannelSplitter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.lang.*;

import org.scijava.util.ByteArray;

import fiji.util.ArrowShape;
import fiji.util.ArrowShape.ArrowStyle;

/**
 * This plugin converts confocal images into a montage utilising the
 * montagemaker plugin
 */
public class Coloc_Marker2 implements PlugIn {
    private static final int HORIZONTAL = 1, VERTICAL = 0;

    public static ImageProcessor ipChannel1, ipChannel2; // image processors accesable from here
    public static List<TwoDPeak> horizontalPeakList, verticalPeakList;
    //private static List<ThreeDPeak> threeDPeakList;
    private ImagePlus imp;
    private static int channel1 = 1; // ChannelSplitter works from base 1 not 0
    private static int channel2 = 3;

    public void run(String args) {
        // get the current image and duplicate it
        imp = WindowManager.getCurrentImage();
        if (imp == null) {
            error("No image is open");
            return;
        } // if there is no image then cancel plugin
        imp = imp.duplicate();
        List<int[]> peakMaxs = getColocLocations(imp, channel1, channel2);// new ArrayList<int[]>();
        
        System.out.print("========================= Center of Peaks Returned ===================");
        peakMaxs.forEach(location -> System.out.print("\n" + location [0] + " " + location[1]));
        
        peakMaxs.forEach(location-> overlayArrow(imp, location[1],location[0]));
        imp.show();
    }
   
    /**
     * Takes an image and two channels to compare.
     * Finds peaks where the two channels in the image colocalise. 
     * Returns a list of locations representing the pixels
     * at the center of these peaks. locations take the form {y,x} 
     * @param imp image to analyse 
     * @param c1 first channel
     * @param c2 second channel
     * @return list of locations of points of colocalisation {y,x}
     */
    public List<int[]> getColocLocations(ImagePlus imp, int c1, int c2){
        List<ThreeDPeak> peaks = getColocPeaks(imp, c1, c2);
        List<int[]> locations = new ArrayList<int[]>();
        peaks.forEach(peak -> locations.add(peak.getPeakMax()));
        return locations;
    }

    public List<ThreeDPeak> getColocPeaks(ImagePlus imp, int c1, int c2){
        List<ThreeDPeak> threeDPeakList;
        List<int[]> colocPeaks = new ArrayList<int[]>();
        ImageProcessor ipCombinedChannels;

        // if imp is less than 2 channels close plugin
        if (imp.getNChannels() < 2) {
            error("There must be more than 2 channels to use this plugin");
            return null;
        }

        // get the two channels for collocalisation
        ipChannel1 = ChannelSplitter.getChannel(imp, c1).getProcessor(1).convertToByte(true); // getProcessor also works from base 1 not 0
        ipChannel2 = ChannelSplitter.getChannel(imp, c2).getProcessor(1).convertToByte(true);

        // ===============create a scaled image processor of combined channels=================
        // create byte array of combined pixel values
        int size = imp.getHeight() * imp.getWidth();
        byte[] ipChannel1Values = (byte[]) ipChannel1.getPixels(), ipChannel2Values = (byte[]) ipChannel2.getPixels();
        byte[] ipCombinedChannelByteValues = new byte[size];
        // average pixel values at each location
        for (int index = 0; index < Math.max(ipChannel1Values.length, ipChannel2Values.length); index++) {
            int a = (index < ipChannel1Values.length ? ipChannel1Values[index] : 0) & 0xff; // 0xff corrects for byte to
                                                                                            // int expansion
            int b = (index < ipChannel2Values.length ? ipChannel2Values[index] : 0) & 0xff;
            int c = (int) Math.sqrt(a * b);
            ipCombinedChannelByteValues[index] = (byte) c;
        }
        // create the combined image processor
        ipCombinedChannels = new ByteProcessor(imp.getWidth(), imp.getHeight(), ipCombinedChannelByteValues);

        /*
         * =============== Do 2D peak picking ======================= 
         * This is based on a peak picking algorithm by Jean-Paul 
         *  https://stackoverflow.com/a/22640362
         */
        // initialise peak lists
        horizontalPeakList = new ArrayList<TwoDPeak>();
        verticalPeakList = new ArrayList<TwoDPeak>();

        // initialise values for algorithm
        int lag = 2; // number of values to include in rolling mean
        int threshold = 2; // number of SDs above mean before counted as peak
        int influence = 0; // influence of each peak value on rolling mean - 1 = same as non signal, 0 = no
                           // influence

        // peak picking function doPeakPick
        TriConsumer<List<Integer>, Integer, Integer> doPeakPick = (dataSet, direction, number) -> { // 'direction' denotes which direction image is being read 'number'
                                                                                                    // indicates what the current row/column is (depends on direction)
            TwoDPeak peakObj = new TwoDPeak(direction);
            List<Integer> filteredDataSet = dataSet; // initialise filtered set
            // initialise filters with 0s
            List<Double> avgFilter = new ArrayList<Double>(dataSet.size());
            Collections.nCopies(dataSet.size(), 0).forEach(value -> avgFilter.add((double) value.intValue()));
            List<Double> stdFilter = new ArrayList<Double>(dataSet.size());
            Collections.nCopies(dataSet.size(), 0).forEach(value -> stdFilter.add((double) value.intValue()));
            avgFilter.set(lag - 1, mean(filteredDataSet.subList(0, lag)));
            stdFilter.set(lag - 1, std(filteredDataSet.subList(0, lag)));

            for (int i = lag; i < dataSet.size(); i++) {
                int[] location = new int[] { (direction == HORIZONTAL ? number : i),
                        (direction == VERTICAL ? number : i) }; // location in the format (row,column)
                if (Math.abs(dataSet.get(i) - avgFilter.get(i - 1)) > threshold * stdFilter.get(i - 1)) {
                    if (dataSet.get(i) > avgFilter.get(i)) {
                        peakObj.addLocation(location);// adds the peak to the peak object
                    }
                    filteredDataSet.set(i, influence * dataSet.get(i) + (1 - influence) * filteredDataSet.get(i - 1));
                    avgFilter.set(i, mean(filteredDataSet.subList(i - lag + 1, i + 1)));
                    stdFilter.set(i, std(filteredDataSet.subList(i - lag + 1, i + 1)));
                } else {
                    if (peakObj.getLocationList().size() > 0) {
                        switch (direction) {
                        case HORIZONTAL:
                            horizontalPeakList.add(peakObj);
                            break;
                        case VERTICAL:
                            verticalPeakList.add(peakObj);
                            break;
                        }

                    }
                    peakObj = new TwoDPeak(direction);
                    filteredDataSet.set(i, dataSet.get(i));
                    avgFilter.set(i, mean(filteredDataSet.subList(i - lag + 1, i + 1)));
                    stdFilter.set(i, mean(filteredDataSet.subList(i - lag + 1, i + 1)));
                }
            }

        }; // doPeakPick function end bracket

        // horizontal peak picking
        int direction = HORIZONTAL;
        for (int i = 0; i < imp.getHeight(); i++) {
            int pixelVal;
            List<Integer> rowValues = new ArrayList<Integer>();
            for (int locInRow = 0; locInRow < ipCombinedChannels.getWidth(); locInRow++) {
                rowValues.add(ipCombinedChannels.getPixel(locInRow, i));
            }
            doPeakPick.accept(rowValues, direction, i);
        }

        // Vertical Peak Picking
        direction = VERTICAL;
        for (int i = 0; i < imp.getWidth(); i++) {
            List<Integer> colValues = new ArrayList<>();
            for (int locInCol = 0; locInCol < ipCombinedChannels.getHeight(); locInCol++) {
                colValues.add(ipCombinedChannels.getPixel(i, locInCol));
            }
            doPeakPick.accept(colValues, direction, i);
        }

        // use the recursive function loadTo3DPeak of the TwoDPeak to assemble 
        // threeDPeak and add it to the list
        threeDPeakList = new ArrayList<ThreeDPeak>();
        horizontalPeakList.forEach(peak -> {
            ThreeDPeak threeDPeak = new ThreeDPeak(ipCombinedChannels);
            peak.loadTo3DPeak(threeDPeak);
            if (threeDPeak.getLocationList().size() > 0 ){
                threeDPeakList.add(threeDPeak);
            }
        });


        threeDPeakList.sort((peak1, peak2) -> {
            return peak2.getLocationList().size() - peak1.getLocationList().size();
        });
        
        return threeDPeakList;
    }

    //====================================== METHODS =================================


    private static void error(String msg) {
        IJ.error("Confocal Montage", msg);
    }

    private static double mean(List<Integer> values) {
        int sum = 0;
        for (int value : values) {
            sum += value;
        }
        return sum / values.size();
    }

    private static double std(List<Integer> values) {
        double sum = 0.0, std = 0.0;
        int length = values.size();
        double mean = mean(values);

        for (double value : values) {
            std += Math.pow(value - mean, 2);
        }

        return Math.sqrt(std / length);

    }
    /**
     * Adds an arrow to the overlay of imp. The tip of the head is placed
     * at the given pixel 
     * @param imp image to which to add arrow
     * @param x x location of arrow head
     * @param y y location of arrow
     * @throws IndexOutOfBoundsException If x or y is greater than width or height respectivley, or less than 0
     */
    public static void overlayArrow(ImagePlus imp, int x , int y){
        Overlay overlay = imp.getOverlay();
        if (overlay == null) overlay = new Overlay();
        imp.setOverlay(overlay);
        int height = imp.getHeight();
        int width = imp.getWidth();

        // check location bounds and throw error if ouside imp dimensions
        if (x >width|| y>height || x<0 ||y<0) throw new IndexOutOfBoundsException("Location x or y a value not contained by the image");

        double arrowLength = (double)width/10;
        ArrowShape arrow = new ArrowShape(ArrowStyle.DELTA, arrowLength);

        arrow.setStartPoint(new Point2D.Double(x-arrowLength,y));
		arrow.setEndPoint(new Point2D.Double(x,y));	
		ShapeRoi arrow_roi = new ShapeRoi(arrow);

        overlay.add(arrow_roi);

    }

    /**
     * Searches a peaklist for the presence of the pixel at the given location and
     * returns the peak in which this pixel resides.
     * 
     * @param location location of the pixel for search in the format [Row,Column]
     * @param peakList peaklist to search
     * @return TwoDPeak containing the pixel or null if not found
     */
    private TwoDPeak lookupPeak(int[] location, List<TwoDPeak> peakList) {
        for (TwoDPeak peak : peakList) {
            List<int[]> peakLocationList = peak.getLocationList();
            ;
            for (int[] peakLocation : peakLocationList) {
                if (peakLocation[0] == location[0] && peakLocation[1] == location[1]) {
                    return peak;
                }
            }
        }
        return null;
    }

    private interface TriConsumer<T, U, R> {
        public void accept(T t, U u, R r);
    }

    //==================================== CLASSES =========================================
    /**
     * Represents a 2D peak found during the Peak Picking algorithm stores the
     * locations of the pixels, present in the peak, in a list
     */
    protected class TwoDPeak extends Peak{
        private int peakDirection;

        private boolean loadTo3DPeakCalled;

        /**
         * initialises object with the direction (HORIZONTAL or VERTICAL) of the peak
         * initialises the Peak variable locaiton List 
         * @param direction direction of 2DPeak (HORIZONTAL or VERTICAL)
         */
        public TwoDPeak(int direction) {
            this.peakDirection = direction;
            loadTo3DPeakCalled = false;
            locationList = new ArrayList<int[]>();
        }

        /**
         * Adds the given location to the location list
         * @param location location to add
         */        
        public void addLocation(int[] location) {
            this.locationList.add(location);
        }

        /**
         * 
         * Adds the locations in this peak to the provided 3D peak 
         * then looks at each location and identifies any 2D peaks it is 
         * associated with. It then calls this function in those objects 
         * with the provided threeDPeak.
         * 
         * @param threeDPeak peak to which to add the locations of TwoDPeak
         */
        public void loadTo3DPeak(ThreeDPeak threeDPeak) {
            if (loadTo3DPeakCalled)
                return;
            loadTo3DPeakCalled = true;
            for (int[] location : this.locationList) {
                // for each location in the list
                // get any peak of opposite direction that is associated with it
                TwoDPeak linkedPeak = null;
                switch (this.peakDirection) {
                case HORIZONTAL:
                    linkedPeak = lookupPeak(location, verticalPeakList);
                    break;
                case VERTICAL:
                    linkedPeak = lookupPeak(location, horizontalPeakList);
                    break;
                }
                // recursively add peaks that are linked to this one
                if (linkedPeak != null)
                    linkedPeak.loadTo3DPeak(threeDPeak);

                // add location to 3DPeak
                threeDPeak.addLocation(location);
            }

            // add peak to 3Dpeak
            threeDPeak.add2DPeak(this);
        }

    }
}
